import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


export interface IzbornoMjesto {
  id: number;
  naziv: String;
  adresa: String;
}

@Injectable({
  providedIn: 'root'
})
export class RezultatiService {

  url: string = 'https://localhost:5001/api/rezultati/';

  constructor(private http: HttpClient) { }

  getIzbornaMjesta(): Observable<IzbornoMjesto[]> {
    return this.http.get<IzbornoMjesto[]>(this.url + 'mjesta');
  }

  getRezultatiZaIzbornoMjesto(imId, izboriId): Observable<IzbornoMjesto[]> {
    return this.http.get<IzbornoMjesto[]>(this.url + imId + '/' + izboriId);
  }

}
