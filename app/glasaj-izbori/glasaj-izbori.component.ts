import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlasanjeService } from '../glasanje.service';
import { GlasajHomeComponent } from '../glasaj-home/glasaj-home.component';
import { MatSnackBar } from '@angular/material';
import { IzboriService } from '../izbori.service';

@Component({
  selector: 'app-glasaj-izbori',
  templateUrl: './glasaj-izbori.component.html',
  styleUrls: ['./glasaj-izbori.component.css']
})
export class GlasajIzboriComponent implements OnInit {

  private kandidature;
  private lastGlasac;

  private selectedKandidat;
  private izbori;

  constructor(private route: ActivatedRoute,
    private glasanjeService: GlasanjeService,
    private izboriService: IzboriService,
    private router: Router,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {

    this.lastGlasac = this.glasanjeService
      .getLastGlasac()
      .subscribe(data => {
        this.lastGlasac = data;

        this.route.paramMap.subscribe(params => {
          let id = +params.get('id');

          this.izboriService
            .getIzbori(id)
            .subscribe(data => this.izbori = data);

          this.glasanjeService
            .getKandidatureZaGlasaca(this.lastGlasac.oib, id)
            .subscribe(data => this.kandidature = data);
        });

      });
  }

  glasajClick() {
    this.glasanjeService
      .posaljiGlas(this.lastGlasac.id, this.selectedKandidat)
      .subscribe(data => {
        if (data > 0) { //if success
          this._snackBar.open("Vaš glas je uspješno poslan.", null, { duration: 3500, panelClass: ['snack-info'] });
          this.router.navigate(['glasaj'])
        }
      });
  }
}
