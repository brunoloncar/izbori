import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { GlasaciService } from '../glasaci.service';

@Component({
  selector: 'app-dodaj-glasaca',
  templateUrl: './dodaj-glasaca.component.html',
  styleUrls: ['./dodaj-glasaca.component.css']
})
export class DodajGlasacaComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router,
    private glasaciService: GlasaciService,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.init();
  }

  init() {

  }

  addGlasac(form) {
    if (form.valid) {
      var glasac = {
        oib: form.value.oib,
        ime: form.value.ime,
        prezime: form.value.prezime
      };

      this.glasaciService.insertGlasac(glasac).subscribe(data => {
        if (data > 0) {
          this._snackBar.open("Glasač je uspješno dodan.", null, { duration: 2500, panelClass: ['snack-info'] });
          this.router.navigate(['admin/glasaci']);
        }
      });

    } else {
      this._snackBar.open("Niste unijeli sva obvezna polja.", null, { duration: 2500, panelClass: ['snack-danger'] });
    }
  }

}
