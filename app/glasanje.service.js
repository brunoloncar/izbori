"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var GlasanjeService = /** @class */ (function () {
    function GlasanjeService(http) {
        this.http = http;
        this.url = 'https://localhost:5001/api/glasaj/';
    }
    GlasanjeService.prototype.getGlasacZaOib = function (oib) {
        return this.http.get(this.url + 'glasac/' + oib);
    };
    GlasanjeService.prototype.getAktualniIzboriZaGlasaca = function (oib) {
        return this.http.get(this.url + 'aktualni/' + oib);
    };
    GlasanjeService.prototype.getKandidatureZaGlasaca = function (oib, izboriId) {
        var newLocal = this.url + 'kandidati/' + oib + '?izboriId=' + izboriId;
        return this.http.get(newLocal);
    };
    GlasanjeService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], GlasanjeService);
    return GlasanjeService;
}());
exports.GlasanjeService = GlasanjeService;
