import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { IzboriService } from '../izbori.service';

@Component({
  selector: 'app-dodaj-izbore',
  templateUrl: './dodaj-izbore.component.html',
  styleUrls: ['./dodaj-izbore.component.css']
})
export class DodajIzboreComponent implements OnInit {

  constructor(private _snackBar: MatSnackBar,
    private izboriService: IzboriService,
    private router: Router) { }

  ngOnInit() {
  }

  addIzbori(form) {
    console.log(form);
    if (form.valid) {
      var izbori = {
        naziv: form.value.naziv,
        odrzavanjeDo: form.value.odrzavanjeDo,
        odrzavanjeOd: form.value.odrzavanjeOd,
        opis: form.value.opis
      }

      this.izboriService.insertIzbori(izbori).subscribe(data => {
        this._snackBar.open("Izbori su uspješno dodani.", null, { duration: 2500, panelClass: ['snack-info'] });
        this.router.navigate(['admin/izbori', data]);
      });

    } else {
      this._snackBar.open("Niste unijeli sva obvezna polja.", null, { duration: 2500, panelClass: ['snack-danger'] });
    }
  }
}
