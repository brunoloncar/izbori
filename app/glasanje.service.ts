import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Izbori, Kandidat } from './izbori.service';

export interface Glasac {
  id: number;
  oib: String;
  ime: String;
  izbornoMjesto: String;
}

@Injectable({
  providedIn: 'root'
})
export class GlasanjeService {

  private url: string = 'https://localhost:5001/api/glasaj/';
  private lastGlasac: Observable<Glasac>;

  getLastGlasac() {
    return this.lastGlasac;
  }

  constructor(private http: HttpClient) { }

  getGlasacZaOib(oib: String): Observable<Glasac> {
    this.lastGlasac = this.http.get<Glasac>(this.url + 'glasac/' + oib);
    return this.lastGlasac;
  }

  getAktualniIzboriZaGlasaca(oib: String): Observable<Izbori[]> {
    return this.http.get<Izbori[]>(this.url + 'aktualni/' + oib);
  }

  getKandidatureZaGlasaca(oib: String, izboriId: number): Observable<Kandidat[]> {
    let url = this.url + 'kandidati/' + oib + '?izboriId=' + izboriId;
    return this.http.get<Kandidat[]>(url);
  }

  posaljiGlas(_glasacId: number, _kandidaturaId: number): Observable<number> {
    return this.http.post<number>(this.url, { glasacId: _glasacId, kandidaturaId: _kandidaturaId });
  }
}
