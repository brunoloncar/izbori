import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RezultatiService } from '../rezultati.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-rezultati-izbora-table',
  templateUrl: './rezultati-izbora-table.component.html',
  styleUrls: ['./rezultati-izbora-table.component.css']
})
export class RezultatiIzboraTableComponent implements OnInit {

  @Input() public rezultati;

  constructor() { }

  ngOnInit() {
    this.init();
  }

  init() {
  }
}
