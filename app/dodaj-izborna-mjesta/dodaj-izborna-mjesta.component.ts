import { Component, OnInit } from '@angular/core';
import { IzboriService } from '../izbori.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-dodaj-izborna-mjesta',
  templateUrl: './dodaj-izborna-mjesta.component.html',
  styleUrls: ['./dodaj-izborna-mjesta.component.css']
})
export class DodajIzbornaMjestaComponent implements OnInit {

  constructor(private izboriService: IzboriService,
    private router: Router,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  addIzbornoMjesto(form) {
    if (form.valid) {
      var izbornoMjesto = {
        naziv: form.value.naziv,
        adresa: form.value.adresa,
      }

      this.izboriService
        .insertIzbornoMjesto(izbornoMjesto)
        .subscribe(data => {
          if (data > 0) {
            this._snackBar.open("Izborno mjesto je uspješno dodano.", null, { duration: 2500, panelClass: ['snack-info'] });
            this.router.navigate(['admin/postavke']);
          }
        });

    } else {
      this._snackBar.open("Niste unijeli sva obvezna polja.", null, { duration: 2500, panelClass: ['snack-danger'] });
    }
  }
}
