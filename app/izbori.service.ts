import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IzbornoMjesto } from './rezultati.service';

export interface Izbori {
  id: number;
  naziv: String;
  opis: String;
  odrzavanjeOdFormat: String;
  odrzavanjeDoFormat: String;
  editable: boolean;
}
export interface Kandidat {
	 id: number,
	 oib: String,
	 ime: String,
	 prezime: String,
}

@Injectable({
  providedIn: 'root'
})
export class IzboriService {

  url: string = 'https://localhost:5001/api/izbori/';

  constructor(private http: HttpClient) { }

  //Izbori
  getIzbori(id: number): Observable<Izbori> {
    return this.http.get<Izbori>(this.url + id);
  }

  getAktualni(): Observable<Izbori[]> {
    return this.http.get<Izbori[]>(this.url + 'aktualni');
  }

  getZavrseni(): Observable<Izbori[]> {
    return this.http.get<Izbori[]>(this.url + 'zavrseni');
  }

  getNadolazeci(): Observable<Izbori[]> {
    return this.http.get<Izbori[]>(this.url + 'nadolazeci');
  }

  insertIzbori(izbori): Observable<number> {
    return this.http.post<number>(this.url + 'dodaj', izbori);
  }

  updateIzbori(izbori): Observable<number> {
    return this.http.put<number>(this.url, izbori);
  }


  //Izborna mjesta
  getAllIzbornaMjesta(): Observable<any[]> {
    return this.http.get<IzbornoMjesto[]>(this.url + 'mjesta/all');
  }

  getIzbornoMjesto(id): Observable<any> {
    return this.http.get<IzbornoMjesto>(this.url + 'mjesta/' + id);
  }

  insertIzbornoMjesto(izbornoMjesto): Observable<number> {
    return this.http.post<number>(this.url + 'mjesta', izbornoMjesto);
  }

  updateIzbornoMjesto(izbornoMjesto): Observable<number> {
    return this.http.put<number>(this.url + 'mjesta', izbornoMjesto);
  }


  //Kandidati
  getAllKandidati(): Observable<Kandidat[]> {
    return this.http.get<Kandidat[]>(this.url + 'kandidati/all');
  }

  getKandidatiZaIzbore(id: number): Observable<Kandidat[]> {
    return this.http.get<Kandidat[]>(this.url + id + '/kandidati');
  }

  getAllKandidatiEligibleForAdd(id: number): Observable<Kandidat[]> {
    return this.http.get<Kandidat[]>(this.url + 'kandidati/' + id + '/all');
  }

  insertKandidat(kandidat): Observable<number> {
    return this.http.post<number>(this.url + 'kandidat', kandidat);
  }


  //Kandidatura
  insertKandidatura(kandidatura): Observable<number> {
    return this.http.post<number>(this.url + 'kandidatura', kandidatura);
  }

}
