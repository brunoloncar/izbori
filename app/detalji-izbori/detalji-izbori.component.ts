import { Component, OnInit } from '@angular/core';
import { IzboriService, Izbori } from '../izbori.service';
import { ActivatedRoute } from '@angular/router';
import { RezultatiService } from '../rezultati.service';

@Component({
  selector: 'app-detalji-izbori',
  templateUrl: './detalji-izbori.component.html',
  styleUrls: ['./detalji-izbori.component.css']
})
export class DetaljiIzboriComponent implements OnInit {

  private izbori;
  private kandidati;
  private izbornaMjesta;
  private selectedIzbornoMjesto;
  private rezultati;

  constructor(private route: ActivatedRoute,
    private izboriService: IzboriService,
    private rezultatiService: RezultatiService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.route.paramMap.subscribe(params => {
      let id = +params.get('id');

      this.izboriService
        .getIzbori(id)
        .subscribe(data => this.izbori = data);

      this.izboriService
        .getKandidatiZaIzbore(id)
        .subscribe(data => this.kandidati = data);

      this.rezultatiService
      .getIzbornaMjesta()
      .subscribe(data =>
        this.izbornaMjesta = data
      );
    });
  }

  izbornoMjestoChanged() {
    if (this.selectedIzbornoMjesto == null) {
      this.rezultati = null;
    }
    else {
      this.rezultatiService
        .getRezultatiZaIzbornoMjesto(this.selectedIzbornoMjesto, this.izbori.id)
        .subscribe(data => this.rezultati = data);
    }
  }
}
