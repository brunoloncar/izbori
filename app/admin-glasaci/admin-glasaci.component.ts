import { Component, OnInit } from '@angular/core';
import { GlasaciService } from '../glasaci.service';
import { IzboriService } from '../izbori.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-admin-glasaci',
  templateUrl: './admin-glasaci.component.html',
  styleUrls: ['./admin-glasaci.component.css']
})
export class AdminGlasaciComponent implements OnInit {

  private glasaci;
  private izbornaMjesta;

  constructor(private glasaciService: GlasaciService,
    private izboriService: IzboriService,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.glasaciService
      .getAllGlasaci()
      .subscribe(data => { this.glasaci = data; });

    this.izboriService
      .getAllIzbornaMjesta()
      .subscribe(data => { this.izbornaMjesta = data; });
  }

  addGlasacIzbornoMjesto(form) {
    if (form.valid) {
      var glasacIzbornoMjesto = {
        glasacId: form.value.glasacId,
        glasackoMjestoId: form.value.izbornoMjestoId,
        vremenskoRazdobljeDo: form.value.vrijemeDo,
        vremenskoRazdobljeOd: form.value.vrijemeOd
      };
      this.glasaciService.insertGlasacIzbornoMjesto(glasacIzbornoMjesto).subscribe(data => {
        if (data > 0) {
          this._snackBar.open("Glasač je dodan u izborno mjesto.", null, { duration: 2500, panelClass: ['snack-info'] });
          this.glasaciService
            .getAllGlasaci()
            .subscribe(data => { this.glasaci = data; });
        }
      });
    }
    else {
      this._snackBar.open("Niste unijeli sva obvezna polja.", null, { duration: 2500, panelClass: ['snack-danger'] });
    }
  }
}
