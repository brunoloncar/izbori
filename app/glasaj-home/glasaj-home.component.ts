import { Component, OnInit } from '@angular/core';
import { GlasanjeService } from '../glasanje.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-glasaj-home',
  templateUrl: './glasaj-home.component.html',
  styleUrls: ['./glasaj-home.component.css']
})
export class GlasajHomeComponent implements OnInit {

  private glasac;
  private izbori;
  private _oib = "";

  constructor(private glasanjeService: GlasanjeService,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  posaljiOib() {
    this.glasanjeService
      .getGlasacZaOib(this._oib)
      .subscribe(data => {
        if (data != null) {
          this.glasac = data;

          this.glasanjeService
            .getAktualniIzboriZaGlasaca(this._oib)
            .subscribe(data => {
              this.izbori = data;
            });

        } else {
          this._snackBar.open("Za obabrani OIB nema podataka.", null, { duration: 3500, panelClass: ['snack-danger'] });
        }
      });
  }

  odjavaClick() {
    this.glasac = null;
    this.izbori = null;
    this._oib = "";

    this._snackBar.open("Odjavljeni ste iz sustava.", null, { duration: 3500, panelClass: ['snack-info'] });
  }
}
