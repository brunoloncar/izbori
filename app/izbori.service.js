"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var IzboriService = /** @class */ (function () {
    function IzboriService(http) {
        this.http = http;
        this.url = 'https://localhost:5001/api/izbori/';
    }
    IzboriService.prototype.getIzbori = function (id) {
        return this.http.get(this.url + id);
    };
    IzboriService.prototype.getAktualni = function () {
        return this.http.get(this.url + 'aktualni');
    };
    IzboriService.prototype.getZavrseni = function () {
        return this.http.get(this.url + 'zavrseni');
    };
    IzboriService.prototype.getNadolazeci = function () {
        return this.http.get(this.url + 'nadolazeci');
    };
    IzboriService.prototype.getKandidatiZaIzbore = function (id) {
        return this.http.get(this.url + id + '/kandidati');
    };
    IzboriService.prototype.getAllKandidati = function () {
        return this.http.get(this.url + 'kandidati/all');
    };
    IzboriService.prototype.getAllKandidatiEligibleForAdd = function (id) {
        return this.http.get(this.url + 'kandidati/' + id + '/all');
    };
    IzboriService.prototype.getAllIzbornaMjesta = function () {
        return this.http.get(this.url + 'mjesta/all');
    };
    IzboriService.prototype.insertIzbori = function (izbori) {
        return this.http.post(this.url + 'dodaj', izbori);
    };
    IzboriService.prototype.insertKandidat = function (kandidat) {
        return this.http.post(this.url + 'kandidat', kandidat);
    };
    IzboriService.prototype.insertKandidatura = function (kandidatura) {
        return this.http.post(this.url + 'kandidatura', kandidatura);
    };
    IzboriService.prototype.updateIzbori = function (izbori) {
        return this.http.put(this.url, izbori);
    };
    IzboriService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], IzboriService);
    return IzboriService;
}());
exports.IzboriService = IzboriService;
