import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IzboriService } from '../izbori.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-uredi-izbore',
  templateUrl: './uredi-izbore.component.html',
  styleUrls: ['./uredi-izbore.component.css']
})
export class UrediIzboreComponent implements OnInit {

  private izbori;
  private izboriId: number;

  constructor(private route: ActivatedRoute,
    private izboriService: IzboriService,
    private router: Router,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.route.paramMap.subscribe(params => {
      let id = +params.get('id');
      this.izboriId = id;

      this.izboriService
        .getIzbori(id)
        .subscribe(data => this.izbori = data);
    });
  }

  editIzbori(form) {
    if (form.valid) {
      var izbori = {
        id: this.izboriId,
        naziv: form.value.naziv,
        odrzavanjeDo: form.value.odrzavanjeDo,
        odrzavanjeOd: form.value.odrzavanjeOd,
        opis: form.value.opis
      }

      this.izboriService
        .updateIzbori(izbori)
        .subscribe(data => {
          if (data > 0) {
            this._snackBar.open("Promjene su uspješno spremljene.", null, { duration: 2500, panelClass: ['snack-info'] });
            this.router.navigate(['admin/izbori', this.izboriId]);
          } else {
            this._snackBar.open("Dogodila se greška prilikom spremanja promjena.", null, { duration: 3500, panelClass: ['snack-danger'] });
          }

        });

    } else {
      this._snackBar.open("Niste unijeli sva obvezna polja.", null, { duration: 2500, panelClass: ['snack-danger'] });
    }
  }
}
