import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { GlasajHomeComponent } from './glasaj-home/glasaj-home.component';
import { DodajIzboreComponent } from './dodaj-izbore/dodaj-izbore.component';
import { DetaljiIzboriComponent } from './detalji-izbori/detalji-izbori.component';
import { RezultatiIzboraTableComponent } from './rezultati-izbora-table/rezultati-izbora-table.component';
import { GlasajIzboriComponent } from './glasaj-izbori/glasaj-izbori.component';
import { DodajKandidatureComponent } from './dodaj-kandidature/dodaj-kandidature.component';
import { UrediIzboreComponent } from './uredi-izbore/uredi-izbore.component';
import { DisplayIzboriComponent } from './display-izbori/display-izbori.component';
import { DodajGlasacaComponent } from './dodaj-glasaca/dodaj-glasaca.component';
import { AdminGlasaciComponent } from './admin-glasaci/admin-glasaci.component';
import { PostavkeComponent } from './postavke/postavke.component';
import { DodajIzbornaMjestaComponent } from './dodaj-izborna-mjesta/dodaj-izborna-mjesta.component';
import { UrediIzbornaMjestaComponent } from './uredi-izborna-mjesta/uredi-izborna-mjesta.component';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LandingPageComponent,
    AdminHomeComponent,
    GlasajHomeComponent,
    DodajIzboreComponent,
    DetaljiIzboriComponent,
    RezultatiIzboraTableComponent,
    GlasajIzboriComponent,
    DodajKandidatureComponent,
    UrediIzboreComponent,
    DisplayIzboriComponent,
    AdminGlasaciComponent,
    DodajGlasacaComponent,
    PostavkeComponent,
    DodajIzbornaMjestaComponent,
    UrediIzbornaMjestaComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule, MatDatepickerModule, MatNativeDateModule,
    MatFormFieldModule, MatInputModule, MatButtonModule, MatTabsModule,
    MatSnackBarModule, MatMenuModule, MatIconModule, MatCheckboxModule,
    RouterModule.forRoot([
      { path: '', component: LandingPageComponent },
      { path: 'admin', component: AdminHomeComponent },
      { path: 'admin/glasaci', component: AdminGlasaciComponent },
      { path: 'admin/glasaci/dodaj', component: DodajGlasacaComponent },
      { path: 'admin/izbori/dodaj', component: DodajIzboreComponent },
      { path: 'admin/izbori/:id', component: DetaljiIzboriComponent },
      { path: 'admin/izbori/uredi/:id', component: UrediIzboreComponent },
      { path: 'admin/kandidature/dodaj/:id', component: DodajKandidatureComponent },
      { path: 'admin/postavke', component: PostavkeComponent },
      { path: 'admin/mjesta/dodaj', component: DodajIzbornaMjestaComponent },
      { path: 'admin/mjesta/uredi/:id', component: UrediIzbornaMjestaComponent },
      { path: 'glasaj', component: GlasajHomeComponent },
      { path: 'glasaj/izbori/:id', component: GlasajIzboriComponent },
    ])
  ],
  exports: [MatSelectModule, MatFormFieldModule, MatInputModule],
  providers: [MatNativeDateModule],
  bootstrap: [AppComponent],
})
export class AppModule { }
