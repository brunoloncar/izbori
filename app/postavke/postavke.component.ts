import { Component, OnInit } from '@angular/core';
import { IzboriService } from '../izbori.service';

@Component({
  selector: 'app-postavke',
  templateUrl: './postavke.component.html',
  styleUrls: ['./postavke.component.css']
})
export class PostavkeComponent implements OnInit {

  private izbornaMjesta;

  constructor(private izboriService: IzboriService,) { }

  ngOnInit() {

    this.init();
  }
    init(): any {
      this.izboriService
        .getAllIzbornaMjesta()
        .subscribe(data => { this.izbornaMjesta = data; });
    }

  editIzbornoMjesto(id) {
  }

}
