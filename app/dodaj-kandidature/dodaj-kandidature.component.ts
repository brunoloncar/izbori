import { Component, OnInit } from '@angular/core';
import { IzboriService } from '../izbori.service';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dodaj-kandidature',
  templateUrl: './dodaj-kandidature.component.html',
  styleUrls: ['./dodaj-kandidature.component.css']
})
export class DodajKandidatureComponent implements OnInit {

  private izbori: any;
  private kandidati;
  private izbornaMjesta: any;
  private izboriId: number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private izboriService: IzboriService,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.route.paramMap.subscribe(params => {
      let id = +params.get('id');
      this.izboriId = id;

      this.izboriService
        .getIzbori(id)
        .subscribe(data => this.izbori = data);
    });

    this.izboriService
      .getAllKandidatiEligibleForAdd(this.izboriId)
      .subscribe(data => this.kandidati = data);

    this.izboriService
      .getAllIzbornaMjesta()
      .subscribe(data => this.izbornaMjesta = data);
  }

  addPostojeci(form) {
    if (form.valid) {
      var kandidatura = {
        izboriId: this.izboriId,
        kandidatId: form.value.kandidatId,
        izbornoMjestoId: form.value.mjestoId,
      }

      this.izboriService.insertKandidatura(kandidatura).subscribe(data => {
        if (data > 0) {
          this._snackBar.open("Kandidatura je uspješno dodana.", null, { duration: 2500, panelClass: ['snack-info'] });
          this.router.navigate(['admin/izbori', this.izboriId]);
        }
      });

    } else {
      this._snackBar.open("Niste unijeli sva obvezna polja.", null, { duration: 2500, panelClass: ['snack-danger'] });
    }
  }

  addNovi(form) {
    if (form.valid) {
      var kandidat = {
        oib: form.value.oib,
        ime: form.value.ime,
        prezime: form.value.prezime
      }

      this.izboriService.insertKandidat(kandidat).subscribe(data => {
        if (data > 0) {
          var kandidatura = {
            izboriId: this.izboriId,
            kandidatId: data,
            izbornoMjestoId: form.value.mjestoId,
          }

          this.izboriService.insertKandidatura(kandidatura).subscribe(data => {
            if (data > 0) {
              this._snackBar.open("Kandidatura je uspješno dodana.", null, { duration: 2500, panelClass: ['snack-info'] });
              this.router.navigate(['admin/izbori', this.izboriId]);
            }
          });
        }
      });
    } else {
      this._snackBar.open("Niste unijeli sva obvezna polja.", null, { duration: 2500, panelClass: ['snack-danger'] });
    }
  }
}
