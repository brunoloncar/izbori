import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface Glasac {
  id: number,
  oib: String,
  ime: String,
  prezime: String,
}

@Injectable({
  providedIn: 'root'
})
export class GlasaciService {
  
  url: string = 'https://localhost:5001/api/glasaci/';

  constructor(private http: HttpClient) { }

  getAllGlasaci(): Observable<Glasac[]> {
    return this.http.get<Glasac[]>(this.url);
  }

  insertGlasac(glasac): Observable<number> {
    return this.http.post<number>(this.url, glasac);
  }

  insertGlasacIzbornoMjesto(glasacIzbornoMjesto): Observable<number> {
    return this.http.post<number>(this.url + 'izbornomjesto', glasacIzbornoMjesto);
  }


}
