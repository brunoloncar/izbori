import { Component, OnInit } from '@angular/core';
import { IzboriService } from '../izbori.service';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {

  //animations: [
  //  trigger('openClose', [
  //    state('open', style({
  //      opacity: 1,
  //    })),
  //    state('closed', style({
  //      opacity: 0,
  //    })),
  //    transition('open => closed', [
  //      animate('0.35s')
  //    ]),
  //    transition('* => open', [
  //      animate('0.35s')
  //    ]),
  //  ]),
  //],

  private aktualni;
  private zavrseni;
  private nadolazeci;

  constructor(private izboriService: IzboriService) { }

  //isOpen = true;
  //toggle() {
  //  this.isOpen = !this.isOpen;
  //}

  ngOnInit() {
    this.init();
  }

  init() {
    this.izboriService
      .getAktualni()
      .subscribe(data => this.aktualni = data);

    this.izboriService
      .getZavrseni()
      .subscribe(data => this.zavrseni = data);

    this.izboriService
      .getNadolazeci()
      .subscribe(data => this.nadolazeci = data);
  }

}
