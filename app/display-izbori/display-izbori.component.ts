import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-display-izbori',
  templateUrl: './display-izbori.component.html',
  styleUrls: ['./display-izbori.component.css']
})
export class DisplayIzboriComponent implements OnInit {

  @Input() private title: String;
  @Input() private izbori: any;

  constructor() { }

  ngOnInit() {
  }

}
