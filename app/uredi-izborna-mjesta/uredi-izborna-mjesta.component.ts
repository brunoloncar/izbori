import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IzboriService } from '../izbori.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-uredi-izborna-mjesta',
  templateUrl: './uredi-izborna-mjesta.component.html',
  styleUrls: ['./uredi-izborna-mjesta.component.css']
})
export class UrediIzbornaMjestaComponent implements OnInit {

  private im;
  private imId;

  constructor(private route: ActivatedRoute,
    private izboriService: IzboriService,
    private router: Router,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.route.paramMap.subscribe(params => {
      this.imId = +params.get('id');

      this.izboriService
        .getIzbornoMjesto(this.imId)
        .subscribe(data => this.im = data);
    });
  }

  editIzbornoMjesto(form) {
    if (form.valid) {
      var izbornoMjesto = {
        id: this.imId,
        naziv: form.value.naziv,
        adresa: form.value.adresa
      }

      this.izboriService
        .updateIzbornoMjesto(izbornoMjesto)
        .subscribe(data => {
          if (data > 0) {
            this._snackBar.open("Promjene su uspješno spremljene.", null, { duration: 2500, panelClass: ['snack-info'] });
            this.router.navigate(['admin/postavke']);
          } else {
            this._snackBar.open("Dogodila se greška prilikom spremanja promjena.", null, { duration: 3500, panelClass: ['snack-danger'] });
          }
        });

    } else {
      this._snackBar.open("Niste unijeli sva obvezna polja.", null, { duration: 2500, panelClass: ['snack-danger'] });
    }
  }
}
